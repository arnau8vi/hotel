/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : hotel

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-01-29 15:44:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `id_client` int(11) NOT NULL,
  `nif` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `cognom` varchar(255) DEFAULT NULL,
  `nacionalitat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of clients
-- ----------------------------

-- ----------------------------
-- Table structure for habitacions
-- ----------------------------
DROP TABLE IF EXISTS `habitacions`;
CREATE TABLE `habitacions` (
  `id_habitacio` int(11) NOT NULL,
  `capacitat` varchar(255) DEFAULT NULL,
  `planta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_habitacio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of habitacions
-- ----------------------------
INSERT INTO `habitacions` VALUES ('101', '3', '1');
INSERT INTO `habitacions` VALUES ('102', '3', '1');
INSERT INTO `habitacions` VALUES ('103', '3', '1');
INSERT INTO `habitacions` VALUES ('104', '3', '1');
INSERT INTO `habitacions` VALUES ('105', '3', '1');
INSERT INTO `habitacions` VALUES ('106', '3', '1');
INSERT INTO `habitacions` VALUES ('107', '3', '1');
INSERT INTO `habitacions` VALUES ('108', '3', '1');
INSERT INTO `habitacions` VALUES ('109', '3', '1');
INSERT INTO `habitacions` VALUES ('110', '3', '1');
INSERT INTO `habitacions` VALUES ('111', '3', '1');
INSERT INTO `habitacions` VALUES ('112', '3', '1');
INSERT INTO `habitacions` VALUES ('113', '3', '1');
INSERT INTO `habitacions` VALUES ('114', '3', '1');
INSERT INTO `habitacions` VALUES ('115', '3', '1');
INSERT INTO `habitacions` VALUES ('116', '3', '1');
INSERT INTO `habitacions` VALUES ('117', '3', '1');
INSERT INTO `habitacions` VALUES ('118', '3', '1');
INSERT INTO `habitacions` VALUES ('119', '3', '1');
INSERT INTO `habitacions` VALUES ('120', '3', '1');
INSERT INTO `habitacions` VALUES ('201', '3', '2');
INSERT INTO `habitacions` VALUES ('202', '3', '2');
INSERT INTO `habitacions` VALUES ('203', '3', '2');
INSERT INTO `habitacions` VALUES ('204', '3', '2');
INSERT INTO `habitacions` VALUES ('205', '3', '2');
INSERT INTO `habitacions` VALUES ('206', '3', '2');
INSERT INTO `habitacions` VALUES ('207', '3', '2');
INSERT INTO `habitacions` VALUES ('208', '3', '2');
INSERT INTO `habitacions` VALUES ('209', '3', '2');
INSERT INTO `habitacions` VALUES ('210', '3', '2');
INSERT INTO `habitacions` VALUES ('211', '3', '2');
INSERT INTO `habitacions` VALUES ('212', '3', '2');
INSERT INTO `habitacions` VALUES ('213', '3', '2');
INSERT INTO `habitacions` VALUES ('214', '3', '2');
INSERT INTO `habitacions` VALUES ('215', '3', '2');
INSERT INTO `habitacions` VALUES ('216', '3', '2');
INSERT INTO `habitacions` VALUES ('217', '3', '2');
INSERT INTO `habitacions` VALUES ('218', '3', '2');
INSERT INTO `habitacions` VALUES ('219', '3', '2');
INSERT INTO `habitacions` VALUES ('220', '3', '2');
INSERT INTO `habitacions` VALUES ('301', '3', '3');
INSERT INTO `habitacions` VALUES ('302', '3', '3');
INSERT INTO `habitacions` VALUES ('303', '3', '3');
INSERT INTO `habitacions` VALUES ('304', '3', '3');
INSERT INTO `habitacions` VALUES ('305', '3', '3');
INSERT INTO `habitacions` VALUES ('306', '3', '3');
INSERT INTO `habitacions` VALUES ('307', '3', '3');
INSERT INTO `habitacions` VALUES ('308', '3', '3');
INSERT INTO `habitacions` VALUES ('309', '3', '3');
INSERT INTO `habitacions` VALUES ('310', '3', '3');
INSERT INTO `habitacions` VALUES ('311', '3', '3');
INSERT INTO `habitacions` VALUES ('312', '3', '3');
INSERT INTO `habitacions` VALUES ('313', '3', '3');
INSERT INTO `habitacions` VALUES ('314', '3', '3');
INSERT INTO `habitacions` VALUES ('315', '3', '3');
INSERT INTO `habitacions` VALUES ('316', '3', '3');
INSERT INTO `habitacions` VALUES ('317', '3', '3');
INSERT INTO `habitacions` VALUES ('318', '3', '3');
INSERT INTO `habitacions` VALUES ('319', '3', '3');
INSERT INTO `habitacions` VALUES ('320', '3', '3');
INSERT INTO `habitacions` VALUES ('401', '3', '4');
INSERT INTO `habitacions` VALUES ('402', '3', '4');
INSERT INTO `habitacions` VALUES ('403', '3', '4');
INSERT INTO `habitacions` VALUES ('404', '3', '4');
INSERT INTO `habitacions` VALUES ('405', '3', '4');
INSERT INTO `habitacions` VALUES ('406', '3', '4');
INSERT INTO `habitacions` VALUES ('407', '3', '4');
INSERT INTO `habitacions` VALUES ('408', '3', '4');
INSERT INTO `habitacions` VALUES ('409', '3', '4');
INSERT INTO `habitacions` VALUES ('410', '3', '4');
INSERT INTO `habitacions` VALUES ('411', '3', '4');
INSERT INTO `habitacions` VALUES ('412', '3', '4');
INSERT INTO `habitacions` VALUES ('413', '3', '4');
INSERT INTO `habitacions` VALUES ('414', '3', '4');
INSERT INTO `habitacions` VALUES ('415', '3', '4');
INSERT INTO `habitacions` VALUES ('416', '3', '4');
INSERT INTO `habitacions` VALUES ('417', '3', '4');
INSERT INTO `habitacions` VALUES ('418', '3', '4');
INSERT INTO `habitacions` VALUES ('419', '3', '4');
INSERT INTO `habitacions` VALUES ('420', '3', '4');

-- ----------------------------
-- Table structure for reserves
-- ----------------------------
DROP TABLE IF EXISTS `reserves`;
CREATE TABLE `reserves` (
  `id_reserva` int(11) NOT NULL,
  `id_habitacio` varchar(255) DEFAULT NULL,
  `id_client` varchar(255) DEFAULT NULL,
  `data_entrada` varchar(255) DEFAULT NULL,
  `dies_sortida` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_reserva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reserves
-- ----------------------------
INSERT INTO `reserves` VALUES ('1', '101', '1', '2019-01-28', '2019-01-30');
INSERT INTO `reserves` VALUES ('2', '101', '2', '2019-02-01', '2019-01-05');
INSERT INTO `reserves` VALUES ('3', '102', '3', '2019-02-07', '2019-02-10');

-- ----------------------------
-- Table structure for usuaris
-- ----------------------------
DROP TABLE IF EXISTS `usuaris`;
CREATE TABLE `usuaris` (
  `nom_usuari` varchar(11) NOT NULL,
  `contrasenya` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuaris
-- ----------------------------
INSERT INTO `usuaris` VALUES ('admin', '1234');
